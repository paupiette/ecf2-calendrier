# ECF2-calendrier

Installation :

1. Après avoir récupéré le fichier sur git 

- git remote add ecf2claire https://gitlab.com/paupiette/ecf2-calendrier.git

- git clone https://gitlab.com/paupiette/ecf2-calendrier.git

2. Ouvrez le fichier : model/calendrier.sql

3. Rendez-vous à la ligne 4 pour changer votre nom d'utilisateur mysql

4. À partir de votre explorateur de fichiers dans le dossier model du projet ouvrez votre terminal et noter la commande suivante : 

sudo mysql < calendrier.sql

5. Le site est prêt à être visualisé dans votre navigateur

