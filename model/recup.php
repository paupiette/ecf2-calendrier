<?php /*la page dans laquelle je récupere les info en bdd*/?>

<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include('model/connect_bdd.php');

//récupération les timestamp dans la bdd
function recup_rdv_query(){
    global $db;

    $requete=$db->query('SELECT id FROM rdv');

    $tuples = $requete->fetchAll();

    $requete->closeCursor();

    return $tuples;
}

$rdv_pris = recup_rdv_query();
//print_r($rdv_pris);
/* 
    Array ( 
        [0] => Array ( 
            [id] => 1628060400 
            [0] => 1628060400 ) 
        [1] => Array ( 
            [id] => 1628064000 
            [0] => 1628064000 ) 
        [2] => Array ( 
            [id] => 1628074800 
            [0] => 1628074800 ) ) */

/*function timestamp_date($a){
    $rdv = date('d/m/Y', $a).' '.date('H:i:s', $a);
    return $rdv;
}*/

//$h = timestamp_date($rdv_pris[0][0]);
//print_r($h);

    $rdv_date = array();    

for ($i=0; $i < count($rdv_pris); $i++){
    $rdv_date[$i] = $rdv_pris[$i][0];
}

//print_r($rdv_date);

$rdv_bdd = json_encode($rdv_date);

//echo($rdv_bdd);
?>