drop database if exists calendrier;

create database calendrier;
grant all on calendrier.* to 'claire'@'localhost';
flush privileges;

use calendrier;

create table rdv(
    id int(11) not null,
    primary key (id),
    nom varchar(30) not null,
    mail varchar(200) not null,
    tel varchar(10) not null,
    unique (id)
);

insert into rdv values(1628060400, 'jean', 'jean@jean.fr', 0657864533);
insert into rdv values(1628064000, 'yan', 'yan@yan.fr', 0657864544);
insert into rdv values(1628074800, 'jim', 'jim@jim.fr', 0657864588);
insert into rdv values(1628319600, 'anie', 'anie@anie.fr', 0876543219);
insert into rdv values(1628352000, 'anie', 'anie@anie.fr', 0876543219);