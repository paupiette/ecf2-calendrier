<?php /*la page de réservation pour les utilisateurs*/?>

<?php
  include('model/recup.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <title>Réservation</title>
</head>
<body>
    <?php
      include('includes/nav.php');
    ?>

    <main class="contain-fluid">
        <div class="row">

            <!--choix de date-->
        <div class="col">
            <form>
              <div class="input-group">
                  <input type="date" class="form-control" id="date-form">
                  <span class="input-group-btn">
                  <button class="btn btn-outline-secondary" type="button" onclick="afficherCreneau()">Regarder les horaires pour cette date</button>
                  </span>
              </div>
            </form>
            <div id="rdv"></div>
        </div>

        <!--entrer ses info-->
        <div class="col">
            <form action="renvoi.php" method="post">
            <input type="hidden" id="hidden-form" name="timestamp" value="<?php echo $_GET['valeur']?>">
            <div class="form-group">
                <label for="name">Nom</label>
                <input type="text" class="form-control" id="nom-form" name="nom" placeholder="Jean">
            </div>
            <div class="form-group">
                <label for="tel">Numéro de téléphone</label>
                <input type="tel" class="form-control" id="tel-form" name="tel" placeholder="0633011456">
            </div>
            <div class="form-group">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" name="mail" placeholder="jean@gmail.com">
            </div>
            <button class="btn btn-secondary" type="submit">Prendre rendez-vous</button>
            </form>
        </div>

        </div>
    </main>

    <script type="text/javascript">

        function afficherCreneau(){
            
            var rdv_bdd = <?php echo $rdv_bdd ?>;
            //les timestamp en bdd - donc les rdv pris - format json

            var divRdv = document.getElementById('rdv');
            //recup la div dans laquelle les horaires disponibles vont s'afficher.
            divRdv.innerHTML="";//je vide la div avant de la re-remplir.

            var date = document.getElementById('date-form').value;//récupération de la valeur dans l'input type=date
            var date = new Date(date);//transformation en date
            var jourSemaine = date.getDay(); //numéro qui donne le jour de la semaine

            var jourMois = date.getDate();
            var mois = date.getMonth(); //pour le mois janvier commence à 0
            var annee = date.getFullYear();

          if(jourSemaine > 1 && jourSemaine < 6){//entre mardi et vendredi

              var rdvMardiVend = [9, 10, 11, 13, 14, 15, 16, 17];
              //tableau des heures de rdv
             
              for(var i=0; i < rdvMardiVend.length;){
                          
                  var dateRdv = new Date(annee, mois, jourMois, rdvMardiVend[i], 0, 0);
                  //date exact en fonction de la valeur dans l'input et de l'heure de rdv

                  var valeurIn = Date.parse(dateRdv) / 1000;//converssion en timestamp - pour faire la comparaison avec les valeurs en bdd
                  var valeurInput = valeurIn.toString();
                          
                  if(rdv_bdd.includes(valeurInput) == true){
                              
                    i++;

                  }else{

                    var form = document.createElement('form');
                    form.setAttribute("action", "reservez.php");
                    form.setAttribute("method", "get");
                    divRdv.appendChild(form);

                    var input = document.createElement('input');
                    input.setAttribute("type", "hidden");
                    input.setAttribute("name", "valeur");
                    input.setAttribute("value", valeurInput);
                    form.appendChild(input);

                    var submit = document.createElement('button');
                    submit.classList.add('btn');
                    submit.classList.add('btn-secondary');
                    submit.setAttribute("type", "submit");
                    var text = rdvMardiVend[i].toString();
                    submit.innerText =  text +"h";
                    form.appendChild(submit);

                    i++;
                  }
              }        
          }

          if(jourSemaine == 6){//le samedi

              var rdvSam = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18];

              for(var i=0; i < rdvSam.length;){
                var dateRdv = new Date(annee, mois, jourMois, rdvSam[i], 00, 00);

                var valeurIn = Date.parse(dateRdv) / 1000;//converssion en timestamp - pour faire la comparaison avec les valeurs en bdd
                var valeurInput = valeurIn.toString();

                if(rdv_bdd.includes(valeurInput) == true){
                              
                  i++;
          
                }else{
          
                    var form = document.createElement('form');
                    form.setAttribute("action", "reservez.php");
                    form.setAttribute("method", "get");
                    divRdv.appendChild(form);

                    var input = document.createElement('input');
                    input.setAttribute("type", "hidden");
                    input.setAttribute("name", "valeur");
                    input.setAttribute("value", valeurInput);
                    form.appendChild(input);

                    var submit = document.createElement('button');
                    submit.classList.add('btn');
                    submit.classList.add('btn-secondary');
                    submit.setAttribute("type", "submit");
                    var text = rdvSam[i].toString();
                    submit.innerText =  text +"h";
                    form.appendChild(submit);
          
                  i++;
                }
              }
          }
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
</body>
</html>