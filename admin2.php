<?php
    include('model/recup.php');

    $inputValeur = $_POST["daterdv"];    
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <?php
      include('includes/nav.php');
    ?>

    <?php

        $dateAuj = explode("-", $inputValeur);//pour récupérer année-jour-mois séparément

        ?>
            <h2>Rendez-vous du <?php echo $dateAuj[2]; echo " / "; echo $dateAuj[1]; echo " / "; echo $dateAuj[0]; ?></h2>
        <?php

        $timeStampInput = mktime(0,0,0,$dateAuj[1], $dateAuj[2], $dateAuj[0]);
        //je le transform la valeur de l'input récup en timestamp sans les heures
        $transDate = getdate($timeStampInput);
        //je le transforme après en tableau associatif pour m'aider à connaître quel jour de la semaine est sélectionné

        $rdv_possibles = array();

        if($transDate['wday'] > 1 AND $transDate['wday'] < 6){//entre mardi et vendredi
            $rdvMardiVend = [9, 10, 11, 13, 14, 15, 16, 17];
            //tableau des heures de rdv

            for($i=0; $i < count($rdvMardiVend); $i++){
                $ajoutTableau = mktime($rdvMardiVend[$i], 0, 0,$dateAuj[1], $dateAuj[2], $dateAuj[0]);
                //je créer tous les timestamp possible en fonction du jour de la semaine et de la date sélectionnée

                array_push($rdv_possibles, $ajoutTableau);
                //j'ajoute chaque nouvelles valeurs au tableau
            }?>

                <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Heure</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Téléphone</th>
                    </tr>
                </thead>
                <tbody> 

            <?php

            for($y=0; $y < count($rdv_possibles); $y++){

                if(in_array($rdv_possibles[$y], $rdv_date)){

                    $rdvPrevus = $rdv_possibles[$y];
                    $heureRdv = getdate($rdvPrevus);

                    $reponse = $db->query("SELECT * FROM rdv where id='$rdvPrevus'");
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch()){
                        ?>
                            <tr>
                                <td><?php echo $heureRdv['hours']; echo "h";?></td>
                                <td><?php echo $donnees['nom']; ?></td>
                                <td><?php echo $donnees['mail']; ?></td>
                                <td><?php echo $donnees['tel']; ?></td>
                            </tr>
                        <?php
                    } 

                    $reponse->closeCursor(); // Termine le traitement de la requête
                }else{}
            }
            ?>
                </tbody>
            </table>
        <?php
        }


        if($transDate['wday'] == 6){//le samedi
            $rdvSam = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18];

            for($i=0; $i< count($rdvSam); $i++){
                $ajoutTableau = mktime($rdvSam[$i], 0, 0,$dateAuj[1], $dateAuj[2], $dateAuj[0]);
                //je créer tous les timestamp possible en fonction du jour de la semaine et de la date sélectionnée

                array_push($rdv_possibles, $ajoutTableau);
                //j'ajoute chaque nouvelles valeurs au tableau
            }

            ?>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Heure</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Téléphone</th>
                    </tr>
                </thead>
                <tbody>
            <?php

            for($y=0; $y < count($rdv_possibles); $y++){

                if(in_array($rdv_possibles[$y], $rdv_date)){ 

                    $rdvPrevus = $rdv_possibles[$y];
                    $heureRdv = getdate($rdvPrevus);


                    $reponse = $db->query("SELECT * FROM rdv where id='$rdvPrevus'");
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch()){
                        ?>
                            <tr>
                                <td><?php echo $heureRdv['hours']; echo "h";?></td>
                                <td><?php echo $donnees['nom']; ?></td>
                                <td><a href="mailto:<?php $donnees['mail']?>"><?php echo $donnees['mail']; ?></a></td>
                                <td><a class="listButton" href="tel:+33<?php echo $donnees['tel']; ?>"><?php echo $donnees['tel']; ?></a></td>
                            </tr>
                        <?php
                    } 

                    $reponse->closeCursor(); // Termine le traitement de la requête
                }else{}
            }
            ?>            
                    </tbody>
                </table>
            <?php
        }    
    ?>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
</body>
</html>