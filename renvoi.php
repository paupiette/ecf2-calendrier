<?php /*la page par laquelle je renvoie les infos en bdd*/?>


<?php 
  error_reporting(E_ALL);
  ini_set("display_errors", 1);

  include('model/connect_bdd.php');

    $id = $_POST["timestamp"];
    
    $nom=$_POST["nom"];
    $tel=$_POST["tel"];
    $mail=$_POST["mail"];

    $ins=$db->prepare("insert into rdv(id, nom, mail, tel) values (:id, :nom, :mail, :tel)");

    $ins->execute(array(
    'id' => $id,
    'nom' => $nom,
    'mail' => $mail,
    'tel' => $tel
    ));
    
  ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>coiffure</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
  </head>
  <body>
    <?php
      include('includes/nav.php');

      $dateDuRdv = date('j/m/Y', $id);
      $heureDuRdv = date('G:i', $id);
    ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col">
          Votre réservation pour un rendez-vous le <?php echo $dateDuRdv?> à <?php echo $heureDuRdv?> a bien été enregistrer. 
        </div>
        <button type="button" class="btn btn-info"><a class="a-renvoi" href="index.php">Retour à l'accueil</button>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
